|[Documentation](https://iespc.gitlab.io/micropython-spc/namespaces.html)| [Liste des capteurs](doc/modules.md)|
|------------------------------------------------------------------------|-------------------------------------|


# Micropython SPC

Distribution Micropython basée sur le microcontrôleur Raspberry Pi Pico et la gamme de modules Grove, pour une utilisation en Sciences Physiques et Chimiques.

Tous les fichiers, modules et paquets sont regroupés dans le réperoire **/lib** à la racine du système de fichiers :

- Le module [**grove.py**](lib/grove.py) inclut un ensemble de variables et de fonctions (similaires au langage Arduino et  permettant un accès simplifié au matériel) qu'il convient d'importer dans l'espace de nom :  
``` from grove import *```

- Le paquet [**drivers**](lib/drivers) contient une collection disparate de drivers, dont l'utilisation est rendue plus homogène grâce à une *couche d'abstraction* [**\_\_init\_\_.py**](lib/drivers/__init__.py)

- Les [autres modules](lib/) peuvent également être importés selon l'usage

## Micropython

[MicroPython](https://micropython.org/) est une implémentation du langage Python pour les microcontrôleurs. On retrouve une grande partie des fonctionnalités de Python 3, ainsi que quelques améliorations pour les systèmes embarqués.

## Matériel

### Raspberry Pi Pico

Le [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) est un microcontrôleur pour lequel une version spécifique de [Micropython](https://micropython.org/download/rp2-pico/) est disponible, ainsi qu'une [documentation](https://docs.micropython.org/en/latest/rp2/quickref.html) complète.

![carte pico](doc/img/rapsberry-pi-pico.png)

### Grove shield

Le [*Grove Shield for Pi Pico*](https://wiki.seeedstudio.com/Grove_Shield_for_Pi_Pico_V1.0/) est une carte d'interface permettant de raccorder facilement les modules de la [gamme Grove](https://www.seeedstudio.com/category/Grove-c-1003.html) sur une carte Raspberry Pi Pico.  
Tous les capteurs et actionneurs fonctionnant en 3,3 V sont compatibles, ce qui permet de réutiliser un grand nombre de modules destinés à l'Arduino.

![Grove shield](doc/img/grove_shield_fr.png)

### Modules Grove

La liste des capteurs, actionneurs et afficheurs actuellement pris en charge est disponible [***ici***](doc/modules.md)

## Logiciels

### Firmware 

[Installer ou mettre à jour](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html) le firmware micropython sur la carte Raspberry Pi Pico.

### Distribution

Pour copier tous les fichiers de la distribution Micropython SPC sur la carte Pico, on peut utiliser par exemple l'outil d'Adafruit [**ampy**](https://pypi.org/project/adafruit-ampy/) :

```ampy -p /dev/ttyACM0 put ./lib /```

