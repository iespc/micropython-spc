"""
Metrology functions for Micropython.
Version: 0.1 @ 2021/07/23
Author: IESPC (iespc at free.fr)
License: GPL v3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""

def uA(data):
    """
    Returns a tuple of the data's mean and type A standard uncertainty.
    data : iterable containing the data.
    """
    from statistics import mean, pstdev
    from math import sqrt
    try :
        iter(data)
        return (mean(data), pstdev(data)/sqrt(len(data)-1))
    except :
        print("Data non iterable")
    
    

def speedOfSound(t=25, h=50, p=1013.25):
    """Computes the speed of sound in the humid air and returns a value in m/s.
        t: temperature in °C
        h: relative humidity in % ; defaults to 50.
        p: pressure in hPa ; defaults to 1013.25)
    """
    # adapted from: http://resource.npl.co.uk/acoustics/techguides/speedair/
    # constants (for convience and clarity)
    Kelvin = 273.15 # For converting to Kelvin
    import math
    e = math.e
    pi = math.pi
    # ensure all the inputs are floats
    T = float(t)
    if not 0 < t < 30 :
        print("Only accurate between 0°C and 30°C")
    T_kel = T + Kelvin  # ambient temperature in Kelvin
    P = float(p)*100   # convert pressure from hPa to Pa
    if not 7500 < P < 102000 :
        print("Only accurate between 750 and 1020 hPa")
    Rh = float(h)
    if not 0 < Rh < 100 :
        Rh = 50.0
        print("Relative humidity out of bonds ; defaults to 50 %")
    # Molecular concentration of water vapour (ENH) calculated from Rh
    # using Giacomos method by Davis (1991) as implemented in DTU report 11b-1997
    ENH = pi*10**(-8)*P + 1.00062 + T**2*5.6*10**(-7)
    PSV1 = T_kel**2*1.2378847*10**(-5)-1.9121316*10**(-2)*T_kel 
    PSV2 = 33.93711047-6.3431645*10**(3)/T_kel 
    PSV = e**PSV1*e**PSV2 
    H = Rh*ENH*PSV/P  # molecular concentration of water vapour
    Xw = H/100.0  # Mole fraction of water vapour 
    Xc = 400.0*10**(-6)  # Mole fraction of carbon dioxide 
    # Speed calculated using the method of Cramer from
    # JASA vol 93 pg 2510
    C1 = 0.603055*T + 331.5024 - T**2*5.28*10**(-4) + (0.1495874*T + 51.471935 -T**2*7.82*10**(-4))*Xw
    C2 = (-1.82*10**(-7)+3.73*10**(-8)*T-T**2*2.93*10**(-10))*P+(-85.20931-0.228525*T+T**2*5.91*10**(-5))*Xc
    C3 = Xw**2*2.835149 + P**2*2.15*10**(-13) - Xc**2*29.179762 - 4.86*10**(-4)*Xw*P*Xc
    C = C1 + C2 - C3  # speed
    return C
    
def densityOfWater(t, p=1013.25, tap=False, uncertainty=False):    
    """
    Returns the CIPM recommanded density of water between 0 °C and 40 °C, in kg/m^3 (g/L).
    t : temperature in °C.
    p : atmospheric pressure in hPa ; defaults to 1013.25.
    tap : tap water correction ; defaults to False.
    uncertainty : if True, returns a tuple (dow, uncertainty) ; defaults to False.
    """
    t=float(t)
    if t < 0:
        print("No liquid water under 0°C")
        return None
    if t > 40 :
        print("Only accurate between 0°C and 40°C")
    p=float(p)
    if not 940 < p < 1050 :
        p = 1013.25
        print("Pressure out of bonds ; defaults to 1013.25 hPa")
    a5 = 999.974950
    a4 = 69.34881
    a3 = 522528.9
    a2 = 301.797
    a1 = -3.983035
    if tap :
        a5 = 999.972  
    rho = a5*(1-((t+a1)**2*(t+a2))/(a3*(t+a4)))
    if not p==1013.25 :
        c1 = 5.074e-10
        c2 = -3.26e-12
        c3 = 4.16e-15
        Cp = 1 + (c1 + c2*t + c3*t**2)*(p*100-101325)
        rho = Cp*rho
    if not uncertainty :
        return rho
    else :
        b1 = 8.394e-4
        b2 = -1.28e-6
        b3 = 1.10e-7
        b4 = -6.09e-9
        b5 = 1.16e-10
        U = b1 + b2*t + b3*t**2 + b4*t**3 + b5*t**4
        return (rho, U)
