#https://commandlinefanatic.com/cgi-bin/showarticle.cgi?article=art084

class modelisation:
 
    def affine(X, Y):
        long = len(X)
        ave_x = sum([X[i] for i in range(long)]) / long
        ave_y = sum([Y[i] for i in range(long)]) / long

        a = sum([X[l] * (Y[l] - ave_y) for l in range(long)]) / sum([X[l] * (X[l] - ave_x) for l in range(long)])
        b = ave_y - a * ave_x
        return (a, b)
        
    def lineaire(X, Y):
        long = len(X)
        a = sum([X[l]*Y[l] for l in range(long)])/sum([X[l]*X[l] for l in range(long)])
        return a


