def cat(fichier):
    #https://github.com/a-happy-ma-nong/msh/tree/main/msh
    try:
       with open(fichier,"r") as f:
                print(f.read())
    except OSError:
        print("cat: %s: No such file or directory" % fichier)

def cd(path):
    #https://github.com/a-happy-ma-nong/msh/tree/main/msh
    try:
        import uos
        uos.chdir(str(path))
    except FileNotFoundError:
        print("cd: %s: No such file or directory" % path)

def ls():
    #https://github.com/a-happy-ma-nong/msh/tree/main/msh
    import uos
    for i in uos.listdir():
        print(i)

def cpufreq():
    #https://github.com/a-happy-ma-nong/msh/tree/main/msh
    import machine
    print(str(int(machine.freq() / 1000000)) + 'MHz')

def free():
    import uos, gc
    fs_stat = uos.statvfs('/')
    fs_size = fs_stat[0] * fs_stat[2]
    fs_free = fs_stat[0] * fs_stat[3]
    total_mem = fs_size/1024
    used_mem = (fs_size-fs_free)/1024
    free_mem = fs_free/1024
    total_mem_s = "{:.2f} KB".format(total_mem)
    used_mem_s = "{:.2f} KB".format(used_mem)
    free_mem_s = "{:.2f} KB".format(free_mem)
    print("{0:12}{1:^12}{2:^12}{3:^12}{4:^12}".format(*['Memory',
                                                                    'Size', 'Used',
                                                                    'Avail',
                                                                    'Use%']))
    print('{0:12}{1:^12}{2:^12}{3:^12}{4:>8}'.format('FS', total_mem_s,
                                                                used_mem_s, free_mem_s,
                                                                "{:.1f} %".format((used_mem/total_mem)*100)))
    gc.collect()
    mem_alloc = gc.mem_alloc()
    mem_free = gc.mem_free()
    total_mem = (mem_alloc+mem_free)/1024
    used_mem = mem_alloc/1024
    free_mem = mem_free/1024
    total_mem_s = "{:.2f} KB".format(total_mem)
    used_mem_s = "{:.2f} KB".format(used_mem)
    free_mem_s = "{:.2f} KB".format(free_mem)
    print('{0:12}{1:^12}{2:^12}{3:^12}{4:>8}'.format('RAM', total_mem_s,
                                                                used_mem_s, free_mem_s,
                                                                "{:.1f} %".format((used_mem/total_mem)*100)))
