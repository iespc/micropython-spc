"""
Micropython SPC
Raspberry Pi Pico with Grove Shield specific variables and Arduino-like functions.
Author: IESPC (iespc at free.fr)
License: GPL v3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""

from machine import Pin, PWM, I2C, ADC
from statistics import median
import utime

__name__ = "Grove Pico"
__version__ = "0.7.7"

## High state 3.3V
HIGH = 1
## High state 3.3V
ON = 1
## Low state 0V
LOW = 0
## Low state 0V
OFF = 0
## Built-in LED Pin
LED_BUILTIN = 25
## I/O Digital Pin 16 and Grove connector D16
D16=16
## I/O Digital Pin 17
D17=17
## I/O Digital Pin 18 and Grove connector D18
D18=18
## I/O Digital Pin 19
D19=19
## I/O Digital Pin 20 and Grove connector D20
D20=20
## I/O Digital Pin 21
D21=21
## Tuple of exposed I/O Digital Pins
dpins = (D16,D17,D18,D19,D20,D21)
## Tuple of Grove I/O connectors
dcons = (D16,D18,D20)
## Analog In A0 Pin/connector
A0=26
## Analog In A1 Pin/connector
A1=27
## Analog In A2 Pin/connector
A2=28
## ADC Voltage reference (V)
ADC_VOLTAGE_REF = 3.3
## ADC Resolution
ADC_RESOLUTION = 65535
## Tuple of Grove Analog In connectors
acons = (A0,A1,A2)
## Grove I2C Bus 0 Connector (id=0)
I2C0=0
## Grove I2C Bus 1 Connector (id=1)
I2C1=1
## Grove Serial Connector UART0 (TX/RX)
#UART0=(0,1)
UART0=0
## Grove Serial Connector UART1 (TX/RX)
#UART1=(4,5)
UART1=1
## Grove SPI Bus Connector (SCK, MOSI, MISO, CS)
SPI0 = (2, 3, 4, 5)

def constrain(x, xMin, xMax):
    """Constrains a number to be within a range.
    x: the number to constrain.
    xMin: the lower end of the range.
    xMax: the upper end of the range."""
    if x < xMin : 
        return xMin
    elif xMax < x :
        return xMax
    else :
        return x

def remap(val, fromLow, fromHigh, toLow, toHigh):
    """Re-maps a number from one range to another. 
    value: the number to map.
    fromLow: the lower bound of the value’s current range.
    fromHigh: the upper bound of the value’s current range.
    toLow: the lower bound of the value’s target range.
    toHigh: the upper bound of the value’s target range."""
    return (val - fromLow) * (toHigh - toLow) / (fromHigh - fromLow) + toLow

def digitalRead(con):
    """Reads the state from a specified digital pin, and returns HIGH or LOW (1 or 0).
    con : the pin/con you want to read the state."""
    pin = Pin(con, Pin.IN)
    return pin.value()

def digitalWrite(con, value):
    """Writes a HIGH or a LOW state to a digital pin.
    con : the pin/con you want to set the state.
    value : 1 or 0 (HIGH or LOW). """
    pin = Pin(con, Pin.OUT)
    pin.value(value)

def toggle(con):
    """Toggles the state of the output pin from LOW to HIGH or HIGH to LOW.
    con: the pin/con to toggle the state."""
    pin = Pin(con)
    pin.value(not pin.value())

def analogRead(con):
    """Reads the value from the specified analog pin, and returns an integer between 0 and 65535.
    con: the analog input pin/con."""
    adc = ADC(Pin(con))
    return adc.read_u16()

def analogReadV(con):
    """Reads the value from the specified analog pin, and returns a voltage, in Volt.
    con: the analog input pin/con."""
    adc = ADC(Pin(con))
    return ADC_VOLTAGE_REF*adc.read_u16()/ADC_RESOLUTION

def analogWrite(con, duty, freq = 1000):
    """Writes an analog value (PWM wave) to a pin.
    con: the pin/con to write to.
    duty: the duty cycle in range 0-65535.
    freq: the PWM frequency in Hertz, defaults to 1000 Hz."""
    pwm0 = PWM(Pin(con))
    pwm0.freq(freq)
    if duty :
        pwm0.duty_u16(constrain(duty, 0, ADC_RESOLUTION))
    else : 
        pwm0.deinit()

def analogWritePc(con, duty, freq = 1000):
    """Writes an analog value (PWM wave) to a pin, in percent.
    con: the pin/con to write to.
    duty: the duty cycle in range 0-100.
    freq: the PWM frequency in Hertz, defaults to 1000 Hz."""
    duty = constrain(duty, 0, 100)
    analogWrite(con, remap(duty, 0, 100, 0, 65535), freq)

def acquire(obj, mes=10, dec=1 ):
    """Returns the median of 'mes' measures, rounded to 'dec' decimal places.
    obj: function or property
    measures: number of measures (default : 10)
    decimals: decimal places (default : 1)"""
    vals = []
    for i in range(mes):
        if callable(obj):
            v = obj()
        else : v = obj
        vals.append(v)
    return round(median(vals), dec)  

def next_pin(con):
    """Returns next Pin of a given Connector number :
    con D16 -> pin D17 ; con A1 -> pin A0.
    con: the connector name."""
    if con in dcons :
        return dpins[dpins.index(con)+1]
    elif con == A1 or con == A2 :
        return acons[acons.index(con)-1]
    else :
        return None

def delay(ms):
    """Pauses the program for the amount of time (in milliseconds) specified as parameter.""" 
    utime.sleep_ms(ms)
    
def delayMicroseconds(us):
    """Pauses the program for the amount of time (in microseconds) specified by the parameter."""
    utime.sleep_us(us)

def tone(con, frequency, duration=0):
    """Generates a square wave of the specified frequency (and 50% duty cycle) on a pin. 
    A null frequency stops the square wave generation.
    con: the pin/con on which to generate the tone.
    frequency: the frequency of the tone in hertz. (0 to stop)
    duration: the duration of the tone in milliseconds (optional)."""
    try :
        pwm0.freq()
    except : 
        pwm0 = PWM(Pin(con))
        pwm0.duty_u16(32767)
    if frequency == 0 :
        pwm0.deinit()
    else : 
        pwm0.freq(frequency)
        if duration :
            utime.sleep_ms(duration)
            pwm0.deinit()

def scanI2C(id=0):
    """Returns a list of i2c devices adresses, in hexdecimal. 
    id: the i2c bus identifier, I2C0 = 0 or I2C1 = 1."""
    i2c = I2C(id)
    return(list(hex(i) for i in i2c.scan()))

def mountSD(spi_con = SPI0):
    import os
    from machine import SPI
    from drivers import sdcard
    sck, mosi, miso, cs = spi_con
    spi = SPI(0,
              baudrate=1000000,
              polarity=1,
              phase=1,
              bits=8,
              firstbit=SPI.MSB,
              sck=Pin(sck),
              mosi=Pin(mosi),
              miso=Pin(miso))
    sd = sdcard.SDCard(spi, Pin(cs))
    vfs = os.VfsFat(sd)
    os.mount(vfs, "/sd")
