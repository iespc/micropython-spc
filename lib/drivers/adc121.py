"""
Micropython ADC121C021 I2C ADC driver.
Adapted from https://wiki.seeedstudio.com/Grove-I2C_ADC/
"""

class ADC121():
    """Driver for the ADC121C021, Grove-I2C ADC."""
    ADDR_ADC121 = 0x50
    REG_ADDR_RESULT = 0x00
    REG_ADDR_CONFIG = 0x02

    def __init__(self, bus, addr=ADDR_ADC121, volt_ref = 3.3):
        self.bus = bus
        self.addr = addr
        self.volt_ref = volt_ref
        self.bus.writeto_mem(self.addr, self.REG_ADDR_CONFIG, bytes([0x20]))

    def voltage(self):
        """Returns the adc voltage in volts."""
        data_list = self.bus.readfrom_mem(self.addr, self.REG_ADDR_RESULT, 2)
        data = ((data_list[0] & 0x0f) << 8 | data_list[1]) & 0xfff
        return data*3.0*2/4095
