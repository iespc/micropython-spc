# Grove modules for rp2 (Raspberry Pi pico)

import utime, machine
from machine import Pin, I2C
from grove import dcons, dpins

def next_pin(con):
    return dpins[dpins.index(con)+1]

def LCD16x2(i2c_con):
    from .lcd1602 import LCD1602
    i2c = machine.I2C(i2c_con)
    lcd = LCD1602(i2c, 2, 16)
    return lcd
    
def SSD1306(i2c_con, width=128, height=64, address=0x3c):
    from .ssd1306 import SSD1306_I2C
    i2c = machine.I2C(i2c_con)
    return SSD1306_I2C(width, height, i2c, address)

def Digit4(con):
    from .tm1637 import TM1637
    return TM1637(Pin(con), Pin(next_pin(con)))

def Neopixel(con, pixels=1):
    import neopixel
    return neopixel.NeoPixel(Pin(con), pixels)

def DHT11(con):
    from dht import DHT11
    return DHT11(Pin(con))

def DHT22(con):
    from dht import DHT22
    return DHT22(Pin(con))

def PCF8574(i2c_con, address = 0x20):
    import pcf8574
    i2c = machine.I2C(i2c_con)
    pcf = pcf8574.PCF8574(i2c, address)
    return pcf

class ADC121():
    def __init__(self, i2c_con) :
        from .adc121 import ADC121
        i2c = machine.I2C(i2c_con)
        self._adc = ADC121(i2c)
        
    def voltage(self):
        return self._adc.voltage()




class INA219():
    """Driver for the INA219 Zerø-Drift, Bidirectional Current/Power Monitor With I2C Interface."""
    def __init__(self, i2c_con, shunt_ohms = 0.1, address = 0x40):
        """The initializer takes the parameters of the INA219 :
        i2c_con: I2C connector.
        shunt_ohms: Shunt resistor value in Ohm. Defaults to `0.1`
        address: I2C address. Defaults to `0x40`"""
        from . import ina219
        i2c = machine.I2C(i2c_con)
        self._ina = ina219.INA219(shunt_ohms, i2c, address = address)
        self._ina.configure()
        
    def voltage(self):
        """Return the bus voltage in volts."""
        return self._ina.voltage() 

    def current(self):
        """Return the bus current in amps.
        A DeviceRangeError exception is thrown if current overflow occurs."""
        return self._ina.current()/1000
        
    def power(self):
        """Return the bus power consumption in watts.
        A DeviceRangeError exception is thrown if current overflow occurs."""
        return self._ina.power()/1000

class TMG39931():
    def __init__(self, i2c_con, gain=1, address=0x39, atime=712):
        from .tmg39931 import TMG39931
        i2c = machine.I2C(i2c_con)
        self._tmg = TMG39931(i2c, gain, address, atime)
    
    def luminance(self):
        lum = self._tmg.luminance()
        return (lum[1], lum[2], lum[3], lum[0])
    
    def illuminance(self):
        return self._tmg.lux()    

    def proximity(self):
        return self._tmg.proximity()  

class BMP180():
    def __init__(self, i2c_con) :
        from .bmp180 import BMP180
        i2c = machine.I2C(i2c_con)
        self._bmp = BMP180(i2c)
        self._bmp.oversample_setting = 3
        self._bmp.baseline = 101325.0
        
    def temperature(self):
        return self._bmp.temperature
    
    def pressure(self):
        return self._bmp.pressure
    
    def measure(self):
        return (self._bmp.temperature, self._bmp.pressure)


class BMP085(BMP180):
    def __init__(self, i2c_bus=None):
        super().__init__(i2c_bus)

class MPX5700AP():
    def __init__(self, apin) :
        self._adc = ADC(Pin(apin))
        
    def pressure(self):
        Vout = adc.read_u16()  
        P = (Vout/65535*777.73)-31.11
        return(int(round(P*10,0)))
    
class Thermistor():
    def __init__(self, pin) :
        from .ntc import NTC
        self._th = NTC(pin, 10000, 10000, 25, 3950, high_side=True)
          
    def temperature(self):
        return self._th.temperature
    
    def resistance(self):
        return self._th.resistance

class TemperatureSensor():
    def __init__(self, pin) :
        from .ntc import NTC
        self._th = NTC(pin, 100000, 100000, 25, 4275, high_side=True)
           
    def temperature(self):
        return self._th.temperature
    
    def resistance(self):
        return self._th.resistance

class HX711():    
    def __init__(self, con):
        from . import hx711
        self._hx = hx711.HX711(d_out=next_pin(con), pd_sck=con)
        
    def measure(self):
        return self._hx.read()

    
class MAX44009():
    def __init__(self, i2c_con):
        from . import max44009
        i2c = machine.I2C(i2c_con)
        self._max = max44009.MAX44009(i2c)

    def illuminance(self):
        return self._max.illuminance_lux

class MPRLS():    
    def __init__(self, i2c_con, address=0x18):
        from . import mprls
        i2c = machine.I2C(i2c_con)
        self._mprls = mprls.MPRLS(i2c, p_min=0, p_max=1724, address=address)
        
    def pressure(self):
        return self._mprls.read()

class MCP4725():
    def __init__(self, i2c_con, address=0x60):
        from . import mcp4725
        from grove import ADC_VOLTAGE_REF
        i2c = machine.I2C(i2c_con)
        self._mcp = mcp4725.MCP4725(i2c, address)

    def write(self, value):
        self._mcp.write(value)

    def voltage(self, U):
        self.write(int(U*4096/ADC_VOLTAGE_REF))
    
class ADS1110():
    def __init__(self, i2c_con, address=0x48):
        from . import ads1110
        i2c = machine.I2C(i2c_con)
        self._ads = ads1110.ADS1110(i2c, address)
        self._rates = {240: 0, 60: 1, 30: 2, 15: 3}
        self._gains = {1: 0, 2: 1, 4: 2, 8: 3}
    
    def voltage(self, gain=1, rate=15):
        raw = self._ads.read(self._rates[rate], self._gains[gain])
        return 2.048*raw/32767/gain


class ADS1115():    
    def __init__(self, i2c_con, gain=1, address=0x48):
        from .ads1x15 import ADS1115
        i2c = machine.I2C(i2c_con)
        self._ads = ADS1115(i2c, address)
        self.gains = {0: 2/3, 1: 1, 2: 2, 3: 4, 4: 8, 5: 16}
        self.set_gain(gain)
        
    def set_gain(self, gain):
        if not 0 <= gain <= 5 :
            gain = 1
        self._ads.gain = gain
        self._gain=self.gains[gain]
        #print(self._gain)
        
    def read(self, channel=0):
        if 0 <= channel <= 3 :
            raw=self._ads.read(channel1 = channel, channel2 = None)
        elif channel == 4 :
            raw=self._ads.read(channel1 = 0, channel2 = 1)
        elif channel == 5 :
            raw=self._ads.read(channel1 = 2, channel2 = 3)
        return raw

    def voltage(self, channel=0):
        volt=(self.read(channel)/32767)*(4.096/self._gain)
        return volt

class ADS1015():    
    def __init__(self, i2c_con, gain=1, address=0x48):
        from .ads1x15 import ADS1015
        i2c = machine.I2C(i2c_con)
        self._ads = ADS1015(i2c, address)
        self.gains = {0: 2/3, 1: 1, 2: 2, 3: 4, 4: 8, 5: 16}
        self.set_gain(gain)
        
    def set_gain(self, gain):
        if not 0 <= gain <= 5 :
            gain = 1
        self._ads.gain = gain
        self._gain=self.gains[gain]
        #print(self._gain)
        
    def read(self, channel=0):
        if 0 <= channel <= 3 :
            raw=self._ads.read(channel1 = channel, channel2 = None)
        elif channel == 4 :
            raw=self._ads.read(channel1 = 0, channel2 = 1)
        elif channel == 5 :
            raw=self._ads.read(channel1 = 2, channel2 = 3)
        return raw
    
    def voltage(self, channel=0):
        volt=(self.read(channel)/2047)*(4.096/self._gain)
        return volt

class DS18X20():
    def __init__(self, con):
        import onewire, ds18x20
        self._ds_roms = []
        self._con = Pin(con)
        self._ds = ds18x20.DS18X20(onewire.OneWire(self._con))
        self.scan()

    def scan(self):
        self._ds_roms = self._ds.scan()
        print("ds18x20:", len(self._ds_roms)) 
            
    def temperature(self, ds_num=0):
        self._ds.convert_temp()
        utime.sleep_ms(750)
        return self._ds.read_temp(self._ds_roms[ds_num])
 
    def roms(self):
        return list(hex(int.from_bytes(x, 'little')) for x in self._ds_roms)  
    
class HCSR04():
    def __init__(self, con):
        from .hcsr04 import HCSR04
        t_pin = con
        e_pin = next_pin(con)
        self._sensor = HCSR04(trigger_pin=t_pin, echo_pin=e_pin)
        
    def duration(self):
        t = self._sensor._send_pulse_and_wait()
        return t
    
    def distance(self, unit = 'cm'):
        if unit == 'cm':
            d = self._sensor.distance_cm()
        else :
            d = self._sensor.distance_mm()
        return d

class UltrasonicRanger():
    def __init__(self, con):
        from .usranger import USRANGER
        self._sensor = USRANGER(con)
    
    def duration(self):
        t = self._sensor._send_pulse_and_wait()
        return t
    
    def distance(self, unit = 'cm'):
        if unit == 'cm':
            d = self._sensor.distance_cm()
        else :
            d = self._sensor.distance_mm()
        return d

class ChainableRGBLed():
    def __init__(self, con):
        from .p9813  import P9813
        _clk = Pin(con, Pin.OUT)
        _data = Pin(next_pin(con), Pin.OUT)
        self._chain = P9813(_clk, _data, 1)

    def color(self, color, led=0):
        self._chain[led] = color
        self._chain.write()

class BH1750():
    def __init__(self, i2c_con):
        from .bh1750 import BH1750 
        i2c = machine.I2C(i2c_con)
        self._bh = BH1750(i2c)
        
    def illuminance(self):
        return self._bh.luminance(self._bh.ONCE_HIRES_1)

class TSL2561():
    def __init__(self, i2c_con):
        from .tsl2561 import TSL2561 
        i2c = machine.I2C(i2c_con)
        self._tsl = TSL2561(i2c, address = 0x29)
        
    def illuminance(self):
        return self._tsl.read()

class BME680():
    def __init__(self, i2c_con):
        from .bme680 import BME680
        i2c = machine.I2C(i2c_con)
        self._bme =  BME680_I2C(i2c)

    def pressure(self):
        return self._bme.pressure
    
    def temperature(self):
        return self._bme.temperature
    
    def humidity(self):
        return self._bme.humidity
    
    def gas(self):
        return self._bme.gas

class MLX90614():
    def __init__(self, i2c_con):
        from .mlx90614 import MLX90614
        i2c = machine.I2C(i2c_con, freq=100000)
        self._mlx =  MLX90614(i2c, address=0x5a)

    def ambient_temperature(self):
        return self._mlx.ambient_temp
    
    def object_temperature(self):
        return self._mlx.object_temp

class BME280():
    def __init__(self, i2c_con):
        from . import bme280_float as bme280
        i2c = machine.I2C(i2c_con)
        self._bme =  bme280.BME280(i2c = i2c)

    def pressure(self):
        return self._bme.read_compensated_data()[0]
    
    def temperature(self):
        return self._bme.read_compensated_data()[1]
    
    def humidity(self):
        return self._bme.read_compensated_data()[2]
    
    def measure(self):
        (t, p, h) = self._bme.read_compensated_data()
        return (t, p, h)

class BMP280():
    def __init__(self, i2c_con):
        from . import bmp280
        i2c = machine.I2C(i2c_con)
        self._bme =  bmp280.BMP280(i2c)

    def pressure(self):
        return self._bme.pressure
    
    def temperature(self):
        return self._bme.temperature


class TCS34725():
    def __init__(self, i2c_con):
        from .tcs34725 import TCS34725
        i2c = machine.I2C(i2c_con)
        self._sensor = TCS34725(i2c)
        self._sensor.integration_time = 200
    
    def gamma_255(self, x):
        """ Appliquer une correction gamma sur une valeur entre 0 - 255 """
        x /= 255
        x = pow(x, 2.5)
        x *= 255
        return int(x) if x < 255 else 255

    def gamma_color(self, color):
        """ Appliquer la correction gamma à un tuple de couleur rgb.
        Les yeux ne perçoivent pas les gammes de couleur de façon linéaire """
        return self.gamma_255(color[0]), self.gamma_255(color[1]), self.gamma_255(color[2])
    
    def color(self):
        return self._sensor.color_rgb_bytes    # color_rgb_bytes
    
    def color_gamma(self):
        rgb = self._sensor.color_rgb_bytes
        return self.gamma_color(rgb)  # Appliquer correction Gamma
    
    def illuminance(self):
        return self._sensor.lux

class SGP30():
    def __init__(self, i2c_con):
        from .adafruit_sgp30 import Adafruit_SGP30
        i2c = machine.I2C(i2c_con)
        self._sgp = Adafruit_SGP30(i2c)
        self._sgp.iaq_init()
        print("Waiting 15 seconds for SGP30 initialization...")
        utime.sleep(15)
    
    @property
    def serial(self):
        return self._sgp.serial
    
    def measure(self):
        return self._sgp.iaq_measure()
    
    def set_baseline(self, co2_baseline, tvoc_baseline):
        self._sgp.set_iaq_baseline(co2_baseline, tvoc_baseline)
    
    def get_baseline(self):
        return self._sgp.get_iaq_baseline()


class NMEA():    
    def __init__(self, uart_con, baudrate = 9600):
        import uasyncio
        self._asyncio = uasyncio
        from machine import UART
        self._uart = UART(int(uart_con), baudrate)
        self._loop = uasyncio.get_event_loop()
        self._loop.create_task(self.main())

    async def main(self):
        reader = self._asyncio.StreamReader(self._uart)
        num = 0
        while num < self._num:
            res = await reader.readline()
            print(res.strip().decode())
            num += 1
        self._loop.stop()
            
    def read(self, num = 20):
        self._num = num
        self._loop.run_forever()
