import machine, time
from machine import Pin

# Original Author
__version__ = '0.2.0'
__author__ = 'Roberto Sánchez'
__license__ = "Apache License 2.0. https://www.apache.org/licenses/LICENSE-2.0"

class USRANGER:
    """
    Driver to use the Grove Ultrasonic Ranger.
    The sensor range is between 2cm and 4m.
    The timeouts received listening to echo pin are converted to OSError('Out of range')

    """
    # echo_timeout_us is based in chip range limit (400cm)
    def __init__(self, pin , echo_timeout_us=500*2*30):
        """
        pin: Output pin to send pulses and to measure the distance.
        By default is based in sensor limit range (4m)
        """
        self.echo_timeout_us = echo_timeout_us
        self.pin = Pin(pin, mode=Pin.OUT)
        self.pin.value(0)

    def _send_pulse_and_wait(self):
        """
        Send the pulse to trigger and listen on echo pin.
        We use the method `machine.time_pulse_us()` to get the microseconds until the echo is received.
        """
        # Init trigger pin (out)
        self.pin.init(Pin.OUT)
        self.pin.value(0) # Stabilize the sensor
        time.sleep_us(5)
        # Send a 10us pulse.
        self.pin.value(1)
        time.sleep_us(10)
        self.pin.value(0)
        # Init echo pin (in)
        self.pin.init(Pin.IN)
        try:
            pulse_time = machine.time_pulse_us(self.pin, 1, self.echo_timeout_us)
            return pulse_time
        except OSError as ex:
            if ex.args[0] == 110: # 110 = ETIMEDOUT
                raise OSError('Out of range')
            raise ex

    def duration(self):
        return self._send_pulse_and_wait()
    
    def distance_mm(self):
        """
        Get the distance in milimeters without floating point operations.
        """
        pulse_time = self._send_pulse_and_wait()

        # To calculate the distance we get the pulse_time and divide it by 2 
        # (the pulse walk the distance twice) and by 29.1 becasue
        # the sound speed on air (343.2 m/s), that It's equivalent to
        # 0.34320 mm/us that is 1mm each 2.91us
        # pulse_time // 2 // 2.91 -> pulse_time // 5.82 -> pulse_time * 100 // 582 
        mm = pulse_time * 100 // 582
        return mm

    def distance_cm(self):
        """
        Get the distance in centimeters with floating point operations.
        It returns a float
        """
        pulse_time = self._send_pulse_and_wait()

        # To calculate the distance we get the pulse_time and divide it by 2 
        # (the pulse walk the distance twice) and by 29.1 becasue
        # the sound speed on air (343.2 m/s), that It's equivalent to
        # 0.034320 cm/us that is 1cm each 29.1us
        cms = (pulse_time / 2) / 29.1
        return cms

