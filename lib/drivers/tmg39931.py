import time

class TMG39931():
    """Micropython TMG39931 - Light Gesture Color Proximity Sensor driver"""

    def __init__(self, bus, gain=1, addr=0x39, atime = 712):
        self.bus = bus
        self.gain = gain
        self.addr = addr
        self.atime = atime
        gains = {1 : b'\x00', 4 : b'\x01', 16 : b'\x02', 64 : b'\x03'}
        atimes = {103 : b'\xDB', 178 : b'\xC0', 712 : b'\x00'}
        # Select Enable register, 0x80(128)
        #		0x0F(15)	Power ON, ALS enable, Proximity enable, Wait disable
        bus.writeto_mem(addr, 0x80, b'\x0F')
        # Select ADC integration time register, 0x81(129)
        #		0x00(00)	ATIME : 712ms, Max count = 65535
        #		0xC0(192)	ATIME : 178ms, Max count = 65535
        #		0xDB(219)	ATIME : 103ms, Max count = 37888
        bus.writeto_mem(addr, 0x81, atimes[atime])
        # Select Wait time register, 0x83(131)
        #		0xFF(255)	WTIME : 2.78ms
        bus.writeto_mem(addr, 0x83, b'\xFF')
        # Select Control register, 0x8F(143)
        #		0x00(00)	AGAIN is 1x
        bus.writeto_mem(addr, 0x8F, gains[gain])
        time.sleep(2)
        
    def _read_data(self):
        # Read data back from 0x94(148), 9 bytes
        return self.bus.readfrom_mem(self.addr, 0x94, 9)
    
    def luminance(self):
        data = self._read_data()
        C = data[1] << 8 | data[0]
        R = data[3] << 8 | data[2]
        G = data[5] << 8 | data[4]
        B = data[7] << 8 | data[6]
        return(C,R,G,B)
    
    def proximity(self):
        data = self._read_data()
        proximity = data[8]
        return proximity
    
    def lux(self):
        C,R,G,B = self.luminance()
        IR = R + G + B
        IR = (IR - C) / 2
        if IR < 0 : IR = 0
        Y = 0.362 * (R - IR) + 1 * (G - IR) + 0.136 * (B - IR)
        ms = self.atime
        gain = self.gain
        CPL = ms * gain / 412
        L = Y / CPL
        return L
