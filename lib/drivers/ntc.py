"""
Adapted from Adafruit-thermistor : 
https://github.com/adafruit/Adafruit_CircuitPython_Thermistor.git
Author: Scott Shawcroft for Adafruit Industries - 2017
"""

from math import log
from machine import Pin, ADC
from grove import ADC_RESOLUTION

class NTC:
    """ 
    This driver takes the parameters of the thermistor and its series resistor to determine the resistance and the temperature. 
    The thermistor is connected either high side (from analog input up to Vcc) or low side (from analog input down to ground).
    """

    def __init__(
        self,
        pin,
        series_resistor,
        nominal_resistance,
        nominal_temperature,
        b_coefficient,
        *,
        high_side=True
    ):
        """
        The initializer takes the parameters of the thermistor, its series resistor, and an optional high_side boolean that defaults to True.
        pin: the analog input pin/con used for the thermistor.
        series_resistor: resistance in series
        nominal_resistance: nominal resistance of the thermistor. normally 10k.
        b_coefficient: thermistor's B coefficient. Typically this is a value in the range of 3000-4000.
        high_side: indicates if the thermistor is connected on the high side or low side of the resistor. Defaults to `True`
        """
        self.adc = ADC(Pin(pin))
        self.series_resistor = series_resistor
        self.nominal_resistance = nominal_resistance
        self.nominal_temperature = nominal_temperature
        self.b_coefficient = b_coefficient
        self.high_side = high_side
        
    @property
    def resistance(self):
        """Returns the resistance of the thermistor in Ohm"""
        if self.high_side:
            # Thermistor connected from analog input to high logic level.
            R = self.series_resistor*(ADC_RESOLUTION / self.adc.read_u16() - 1.0)
        else:
            # Thermistor connected from analog input to ground.
            R = self.series_resistor / (ADC_RESOLUTION / self.adc.read_u16() - 1.0)
        return R
    
    @property    
    def temperature(self):
        """Returns the temperature of the thermistor in degrees Celsius"""
        R = self.resistance
        steinhart = R / self.nominal_resistance  # (R/Ro)
        steinhart = log(steinhart)  # ln(R/Ro)
        steinhart /= self.b_coefficient  # 1/B * ln(R/Ro)
        steinhart += 1.0 / (self.nominal_temperature + 273.15)  # + (1/To)
        steinhart = 1.0 / steinhart  # Invert
        steinhart -= 273.15  # convert to C
        return steinhart
