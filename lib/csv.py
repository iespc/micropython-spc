"""
Minimal CSV reader/writer for Micropython.
Version: 0.1 @ 2021/07/16
Author: IESPC (iespc at free.fr)
License: GPL v3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
def isFloat(value):
    """Returns a float if possible or a string.
    value : string to convert to float"""
    try:
        return float(value)
    except ValueError:
        return value

class writer():
    """Class to write CSV files.""" 

    def __init__(self, csvfile):
        """Opens the CSV file for appending."""
        self.csvfile = open (csvfile, "a")
    def writerow(self, row):
        """Appends a row to the CSV file.
        row : iterable (list or tuple)"""
        self.csvfile.write(",".join([str(r) for r in row]))
        self.csvfile.write("\n")
    def close(self):
        """Close the CSV file"""
        self.csvfile.close() 

class reader():
    """Class to read CSV files.""" 
    def __init__(self, csvfile):
        """Opens the CSV file for reading and extracts rows"""
        lines = [x.rstrip() for x in open(csvfile)]
        self.rows = [[isFloat(x) for x in lines[l].split(',')] for l in range(len(lines))]
    def __repr__(self):
        """Returns a list of rows"""
        return self.rows
