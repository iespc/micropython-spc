# https://stm32python.gitlab.io/fr/docs/Micropython/grove/gps

from machine import UART
import utime as time
from drivers import adafruit_gps

uart = UART(0, 9600, timeout = 5000)
gps = adafruit_gps.GPS(uart)
# Renvoie les trames GGA et RMC :
gps.send_command('PMTK314,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')
gps.send_command('PMTK220,1000') # 1000 => 1000 ms <=> 1s
last_print = time.ticks_ms() # Mesure du temps initial

print("Acquisition des satellites en cours...")

while True:
	gps.update()
	current = time.ticks_ms()
	if current-last_print >= 1000:
		last_print = current
		if gps.has_fix:
			print('=' * 40)
			print('Horodatage acquisition: {}/{}/{} {:02}:{:02}:{:02}'.format(
				gps.timestamp_utc[1],
				gps.timestamp_utc[2],
				gps.timestamp_utc[0],
				gps.timestamp_utc[3],
				gps.timestamp_utc[4],
				gps.timestamp_utc[5]))
			print('Latitude: {} degrés'.format(gps.latitude))
			print('Longitude: {} degrés'.format(gps.longitude))
			print('Qualité acquisition: {}'.format(gps.fix_quality))
			if gps.satellites is not None:
				print('# satellites: {}'.format(gps.satellites))
			if gps.altitude_m is not None:
				print('Altitude: {} mètres'.format(gps.altitude_m))