var statistics_8py =
[
    [ "_ss", "statistics_8py.html#af1585f5f5f6113794121f5e2170e2c71", null ],
    [ "harmonic_mean", "statistics_8py.html#a33761b8024a27b4f321e6cddd7a0a36b", null ],
    [ "mean", "statistics_8py.html#a255ac799254c9bfb93ebfc03ebfe0f7b", null ],
    [ "median", "statistics_8py.html#a0769e0b1d7e4b3a7b4157d35e0baf306", null ],
    [ "median_grouped", "statistics_8py.html#aa41c39d7cf6e85f9541e9499ae1f1293", null ],
    [ "median_high", "statistics_8py.html#ad52b524ea7e5545b9524bf0741ab31e9", null ],
    [ "median_low", "statistics_8py.html#a95f9e5b8d97f37bbfb9a061c9d1b52aa", null ],
    [ "mode", "statistics_8py.html#ac31798cc7dacc2e83778660e66aabc5f", null ],
    [ "pstdev", "statistics_8py.html#aab41015af8734ba3d18ab0a10bac9778", null ],
    [ "pvariance", "statistics_8py.html#a6a176cb2b01cbe9b8ac31b56edf84222", null ],
    [ "stdev", "statistics_8py.html#a99e35d355a096fc726c00a7ca1d76c20", null ],
    [ "variance", "statistics_8py.html#a1d1a6f15c8bf34058bc962096960d650", null ]
];