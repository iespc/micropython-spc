var searchData=
[
  ['pressure_175',['pressure',['../classdrivers_1_1BMP180.html#a02ebf49eaedc080af05c0b6b5f719285',1,'drivers.BMP180.pressure()'],['../classdrivers_1_1MPX5700AP.html#a02ebf49eaedc080af05c0b6b5f719285',1,'drivers.MPX5700AP.pressure()'],['../classdrivers_1_1MPRLS.html#a02ebf49eaedc080af05c0b6b5f719285',1,'drivers.MPRLS.pressure()']]],
  ['pstdev_176',['pstdev',['../namespacestatistics.html#aab41015af8734ba3d18ab0a10bac9778',1,'statistics']]],
  ['pvariance_177',['pvariance',['../namespacestatistics.html#a6a176cb2b01cbe9b8ac31b56edf84222',1,'statistics']]]
];
