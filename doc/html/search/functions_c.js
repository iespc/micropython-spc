var searchData=
[
  ['scan_182',['scan',['../classdrivers_1_1DS18X20.html#a4bc2369d2bbe324690e7831a1f65fa4c',1,'drivers::DS18X20']]],
  ['scani2c_183',['scanI2C',['../namespacegrove.html#af68ec11b4c43cc69100f399d11178cf5',1,'grove']]],
  ['set_5fgain_184',['set_gain',['../classdrivers_1_1ADS1115.html#ac47f67c1fda63d3e7ce7dd4cc3ea2d69',1,'drivers.ADS1115.set_gain()'],['../classdrivers_1_1ADS1015.html#ac47f67c1fda63d3e7ce7dd4cc3ea2d69',1,'drivers.ADS1015.set_gain()']]],
  ['speedofsound_185',['speedOfSound',['../namespacemetrology.html#aa7fa10702d85c2889c953d907fa43ac9',1,'metrology']]],
  ['ssd1306_186',['SSD1306',['../namespacedrivers.html#a7cfc7b55d8a549bae7ecffb202f1eccd',1,'drivers']]],
  ['stdev_187',['stdev',['../namespacestatistics.html#a99e35d355a096fc726c00a7ca1d76c20',1,'statistics']]]
];
