var searchData=
[
  ['mean_166',['mean',['../namespacestatistics.html#a255ac799254c9bfb93ebfc03ebfe0f7b',1,'statistics']]],
  ['measure_167',['measure',['../classdrivers_1_1BMP180.html#a041b569662bf6e657f570155c2de2d8c',1,'drivers.BMP180.measure()'],['../classdrivers_1_1DHT11.html#a041b569662bf6e657f570155c2de2d8c',1,'drivers.DHT11.measure()']]],
  ['median_168',['median',['../namespacestatistics.html#a0769e0b1d7e4b3a7b4157d35e0baf306',1,'statistics']]],
  ['median_5fgrouped_169',['median_grouped',['../namespacestatistics.html#aa41c39d7cf6e85f9541e9499ae1f1293',1,'statistics']]],
  ['median_5fhigh_170',['median_high',['../namespacestatistics.html#ad52b524ea7e5545b9524bf0741ab31e9',1,'statistics']]],
  ['median_5flow_171',['median_low',['../namespacestatistics.html#a95f9e5b8d97f37bbfb9a061c9d1b52aa',1,'statistics']]],
  ['mode_172',['mode',['../namespacestatistics.html#ac31798cc7dacc2e83778660e66aabc5f',1,'statistics']]]
];
