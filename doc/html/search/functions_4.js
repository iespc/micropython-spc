var searchData=
[
  ['delay_152',['delay',['../namespacegrove.html#a8faedd4e73b32f1fdc366fcbbff2f5bc',1,'grove']]],
  ['delaymicroseconds_153',['delayMicroseconds',['../namespacegrove.html#a7fbb9b68dada266816b9cb0f7f95f65b',1,'grove']]],
  ['densityofwater_154',['densityOfWater',['../namespacemetrology.html#aeac32a9e7fb37cc28bdb3ba53bc1f663',1,'metrology']]],
  ['digit4_155',['Digit4',['../namespacedrivers.html#a29737981906b1226f49ff7abe9f16777',1,'drivers']]],
  ['digitalread_156',['digitalRead',['../namespacegrove.html#a48f5eb3e37171e33a870566012128262',1,'grove']]],
  ['digitalwrite_157',['digitalWrite',['../namespacegrove.html#a8451cbea9c208cb286f48d371a05e571',1,'grove']]],
  ['distance_158',['distance',['../classdrivers_1_1HCSR04.html#a7f2c769a0cc4c0c0edfef09d97a210e0',1,'drivers.HCSR04.distance()'],['../classdrivers_1_1UltrasonicRanger.html#a7f2c769a0cc4c0c0edfef09d97a210e0',1,'drivers.UltrasonicRanger.distance()']]],
  ['duration_159',['duration',['../classdrivers_1_1HCSR04.html#aa3b12f0667d5334a74c379f87dd4ab6f',1,'drivers.HCSR04.duration()'],['../classdrivers_1_1UltrasonicRanger.html#aa3b12f0667d5334a74c379f87dd4ab6f',1,'drivers.UltrasonicRanger.duration()']]]
];
